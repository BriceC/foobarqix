package fr.diggers.demo.foobarqix;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FooBarQixTest {

    @Test
    public void should_throw_exception_when_not_a_number() {
        Assertions.assertThrows(NumberFormatException.class, () -> FooBarQix.compute("X"));
    }

    @Test
    public void should_return_Foo_when_divisible_by_3() {
        String ret = FooBarQix.compute("6");
        Assertions.assertEquals("Foo", ret);
    }

    @Test
    public void should_return_FooBarBar_when_divisible_by_5_and_3_and_contains_5() {
        String ret = FooBarQix.compute("15");
        Assertions.assertEquals("FooBarBar", ret);
    }

    @Test
    public void should_return_Qix_when_divisible_by_7() {
        String ret = FooBarQix.compute("14");
        Assertions.assertEquals("Qix", ret);
    }

    @Test
    public void should_return_Foo_when_contains_3() {
        String ret = FooBarQix.compute("13");
        Assertions.assertEquals("Foo", ret);
    }

    @Test
    public void should_return_Bar_when_contains_5() {
        String ret = FooBarQix.compute("52");
        Assertions.assertEquals("Bar", ret);
    }

    @Test
    public void should_return_Qix_when_contains_7() {
        String ret = FooBarQix.compute("17");
        Assertions.assertEquals("Qix", ret);
    }

    @Test
    public void should_return_number_when_not_divisible_by_3_5_7_and_not_contains_3_5_7() {
        String ret = FooBarQix.compute("1");
        Assertions.assertEquals("1", ret);
    }

    @Test
    public void should_return_number_with_stars_when_not_divisible_by_3_5_7_and_not_contains_3_5_7() {
        String ret = FooBarQix.compute("101");
        Assertions.assertEquals("1*1", ret);
    }

    @Test
    public void should_return_FooFoo_star_Foo_when_303() {
        String ret = FooBarQix.compute("303");
        Assertions.assertEquals("FooFoo*Foo", ret);
    }
}
