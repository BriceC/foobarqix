package fr.diggers.demo.foobarqix;

public class FooBarQix {

    public static String compute(String input) {
        Integer number = Integer.parseInt(input);
        StringBuilder ret = new StringBuilder();

        ret.append(computeDivisible(number));
        ret.append(computeDigits(input));

        if (isEmptyOrContainsOnlyStar(ret)) {
            return input.replace("0", "*");
        }
        return ret.toString();
    }

    private static boolean isEmptyOrContainsOnlyStar(StringBuilder ret) {
        return ret.isEmpty() || ret.toString().replace("*", "").isEmpty();
    }

    private static StringBuilder computeDivisible(Integer number) {
        StringBuilder ret = new StringBuilder();
        if (isDivisibleBy(number, 3)) {
            ret.append("Foo");
        }
        if (isDivisibleBy(number, 5)) {
            ret.append("Bar");
        }
        if (isDivisibleBy(number, 7)) {
            ret.append("Qix");
        }
        return ret;
    }

    private static StringBuilder computeDigits(String input) {
        StringBuilder ret = new StringBuilder();
        for (char digit : input.toCharArray()) {
            if (digit == '3') {
                ret.append("Foo");
            }
            if (digit == '5') {
                ret.append("Bar");
            }
            if (digit == '7') {
                ret.append("Qix");
            }
            if (digit == '0') {
                ret.append("*");
            }
        }
        return ret;
    }

    private static boolean isDivisibleBy(Integer number, Integer divisor) {
        return number % divisor == 0;
    }

}
